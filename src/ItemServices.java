import java.util.ArrayList;
import java.util.List;

public class ItemServices {
	private List<Item> polozky;
	private FileHandler fh;
	
	public ItemServices() {
		polozky = new ArrayList<>();
		fh = new FileSerializator();
	}
	
	public void addItem(Item item) {
		polozky.add(item);
	}
	
	public void removeItem() {
		
	}
	
	public Item getItem(int i) {
		return polozky.get(i);
	}
	
	public List<Item> getAll() {
		return polozky;
	}
}
