
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileSerializator implements FileHandler {

	@Override
	public List<Item> loadData(String path) throws Exception {
		ArrayList<Item> listToLoad = new ArrayList<>();
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
		listToLoad = (ArrayList<Item>) ois.readObject();
		ois.close();
		return listToLoad;
	}

	@Override
	public void saveData(String path, List<Item> list) throws Exception {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path));
		oos.writeObject(list);
		oos.close();
	}

}
