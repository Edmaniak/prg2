
import java.util.List;

public interface FileHandler {
	public List<Item> loadData(String path) throws Exception;
	public void saveData(String path,List<Item> list) throws Exception;
	
}
